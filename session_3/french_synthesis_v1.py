#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scripts.server_pytorch_resources import TTSInferenceModel, VocoderInferenceModel
import sys
import os
import glob
import yaml
import argparse
import pandas

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("--voice",type=str ,default='synpaflex' ,help="Which voice to use during the synthesizing process. Must be 'neb' or 'synpaflex'")
parser.add_argument("--type",type=str ,default='character', help="What type of input is provided. Must be 'character' or 'phoneme'")
parser.add_argument("input", help="From which file to read the sentences to synthesize.")
parser.add_argument("output", help="In which folder to save the synthesized samples.")
parser.add_argument("--debug", help="Whether to print debug information.", action="store_true", default=False)
args = parser.parse_args()
voice = args.voice
input_text_file = args.input
feats_location = args.output
wav_output_dir = args.output
input_type = args.type
debug = args.debug

# Create output directory if it does not exist yet
os.makedirs(feats_location, exist_ok=True)

if voice == "neb":
    if input_type == "phoneme":
        print("Neb phoneme does not exist")
    elif input_type == "character":
        conf_file = "conf/french.neb.v1.conf"
        if debug:
            print("Neb character")
elif voice == "synpaflex":
    if input_type == "phoneme":
        print("Synpaflex phoneme does not exist")
    elif input_type == "character":
        conf_file = "conf/french.synpaflex.f19.v1.conf"
        if debug:
            print("Synpaflex character")

# Read configuration for synthesis
with open(conf_file) as vocoder_conf_file:
    config = yaml.load(vocoder_conf_file, Loader=yaml.Loader)
    synth_model = config["synth_model"]
    model_json = config["model_json"]
    input_dictionary = config["dict_type"]
    data_json_file = "scripts/data.json"
    model_info = config["vocoder_path"]
    vocoder_conf = config["vocoder_conf"]
    force_cpu = False

# Reading sentences to synthesize
# with open(input_text_file, "r") as opened_file:
#     sentences_to_synthesize = opened_file.readlines()
# for i in range(len(sentences_to_synthesize)):
#     sentences_to_synthesize[i] = sentences_to_synthesize[i].strip("\n")

df_input=pandas.read_csv(input_text_file)

# print(df_input.head())
sentences_to_synthesize=df_input['word'].to_list()

if debug:    
    print(sentences_to_synthesize)
    print("=====================")

# Cleaning
for s in range(len(sentences_to_synthesize)):
    sentence = list(sentences_to_synthesize[s])
    for i in range(len(sentences_to_synthesize[s])):
        character = sentence[i].lower()
        if character == " ":
            character = "<space>"
        sentence[i] = character
    sentences_to_synthesize[s] = sentence
if debug:
    print(sentences_to_synthesize)

# Load the dictionary allowing to translate characters/phonemes into tokens
with open(input_dictionary, "r") as opened_file:
    lines = opened_file.readlines()
    token_dictionary = dict()
    for line in lines:
        split_line = line.strip("\n").split(" ")
        token_dictionary[split_line[0].lower()] = split_line[1]
    if debug:
        print(token_dictionary)
        print(len(token_dictionary))

# Encode the sentences to synthesize as tokens
nb_OOV = 0
encoded_sentences = []
for sentence in sentences_to_synthesize:
    sequence_encoded = []
    for item in sentence:
        if item not in token_dictionary:
            print("{} is not in the dictionary, replacing with <unk> token".format(item), file=sys.stderr)
            nb_OOV += 1
            item = '<unk>'
        sequence_encoded.append(token_dictionary[item])
    encoded_sentences.append(sequence_encoded)
if debug:    
    print("Number of OOV: {}".format(nb_OOV))
    print(encoded_sentences)

#    

# Generating the json object in the format expected by espnet
utts = []
# i = 0
for i,sentence in enumerate(encoded_sentences):
    utts.append(dict({"utts": 
                        dict({"sentence_{}".format(str(i).zfill(5)):
                                dict({
                                      "output": [dict({"name" : "target1",
                                                            "shape": [len(sequence_encoded), len(token_dictionary)],
                                                            "tokenid": " ".join(sentence)
                                                    })]
                                })
                        })
                }))
    # i+=1
if debug:
    print(utts)

# Preparation of the Tacotron model
inference_tts = TTSInferenceModel()
inference_tts.prepare_tts(synth_model, model_json, 0)
load_inputs_and_targets, js = inference_tts.prepare_decoding(data_json_file, feats_location)


# Spectrogram generation
for i in range(len(utts)):
    feat_file_fullpath=os.path.join(feats_location, "sentence_{}".format(str(i).zfill(5)))
    inference_tts.decode(feat_file_fullpath, load_inputs_and_targets, utts[i]["utts"])



    

# Preparation of the ParallelWaveGAN model
inference_vocoder = VocoderInferenceModel()
inference_vocoder.prepare_vocoder(model_info, vocoder_conf, force_cpu)
nb = 0
outfile_list=[]
# Spectrogram to wav conversion
for i in range(len(utts)):
    wav_file = inference_vocoder.decode(os.path.join(feats_location, "sentence_{}.scp".format(str(i).zfill(5))), wav_output_dir, nb)
    outfile_list.append(wav_file)



df_input['{}'.format(voice)]=outfile_list
# Remove unnecessary files
files_to_remove = glob.glob(os.path.join(feats_location, "*.scp"))
files_to_remove.extend(glob.glob(os.path.join(feats_location, "*.ark")))
for a_file in files_to_remove:
    os.remove(a_file)
#save the data 
ofile=os.path.splitext(input_text_file)[0]+'_out.csv'
df_input.to_csv(ofile,sep='\t',index=False)


