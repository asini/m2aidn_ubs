# Session 1.3 - TTS



## How to setup conda environnement

- Create a specific python virtual environment to mitigate conflicts using miniconda:
    ```
    conda create -n m2aidn_talp python=3.8
    ```

- Activate the virtual environment and install the python dependancies to run this projet:
    ```
    conda activate m2aidn
    pip install -r requirements.txt
    ```
 



## Text normalization

- Clone the irisa text normalizer for fr/en language
 
   ```
   	git clone https://github.com/glecorve/irisa-text-normalizer.git
   
   ```
   This perl code source is useful for preparing and designing a corpus for both asr and tts.

## TTS 
   Once the text is normalized we can synthesise the text.	


## Evaluation

Automatic objective evalution using Mel-Cepstral Distorision


